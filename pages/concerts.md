---
layout: concerts
title: CONCERTS
---

# Prochains concerts

* **15 Juin 2024** : Guinguette organisée par **La MAGMA** et la **SMAC07** à Annonay (07)
* **22 Juin 2024** : Fête estivale des Rioux organisée par **Grégou et Caboi** à Andance (07)

# Ils et elles nous ont fait jouer :

* **25 Octobre 2019** : <span id="jo">Jo</span> nous a fait jouer à **La Charrue** à Châteaubriant (44)
* **24 Octobre 2019** : <span id="">Les 3 p'tits Cochons</span> nous ont fait jouer dans leur bar **Les 3 p'tits Cochons** à Bourges (18)
* **23 Octobre 2019** : <span id="herve">Hervé</span> nous a fait jouer au **Bistrot Culture** à Ainay-Le-Château (03)
* **13 Septembre 2019** : <span id="clement">Clément</span> nous a fait jouer à **La Farlodoise** à Chazelles-sur-Lyon (42)
* **7 Septembre 2019** : <span id="mathieu">Mathieu</span> et <span id="pierre">Pierre</span> nous ont enregistré au studio **Purple Sheep** quelque part dans les monts du Lyonnais.
* **31 Août** **2019** : <span id="">La Boîte à Rêves</span> nous ont fait jouer avec nos copain.e.s de L'Affect pour le **Festival Bule** à Quingey (25)
* **19 Avril 2019** : <span id="">Nat'</span> nous a fait jouer au **Bar-Bar** à Annonay (07)
<br>
* **14 Décembre 2018** : <span id="jerem">Jerem nous a fait jouer au festival **Paye ton Noël** au Molodoï à Strasbourg (67)</span>
* **17 Août 2018** : <span id="ugo">Ugo et les nouveaux sauvages</span> nous ont fait jouer au festival **le Live de la Jungle** à Crest (26)
* **7 Juillet 2018** : <span id="didier">Didier</span> nous a fait jouer aux **Flâneries Musicales de Félines** (07)
* **30 Juin 2018** : <span id="lumar_rena">Lumar et Réna</span> nous ont fait jouer à la **soirée de soutien à La Magma à Annonay** (07)
* **23 Juin 2018** : <span id="on">On</span> s’est fait jouer au **Toï-Toï** à Villeurbanne (69)
* **08 Juin 2018** : <span id="anais">Anaïs</span> nous a fait jouer au **Port Nord (La Méandre)** à Châlon sur saône (71)
* **5 Mai 2018** : <span id="victoire">Victoire</span> nous a fait jouer au **Prieuré** à Lyon (69)
* **21 Avril 2018** : <span id="franky_rim">Francky et Rim</span> nous ont fait jouer au **Court Circuit** à Lyon (69)
<br>
* **21 Juin 2017** : <span id="gaspard_laffect">Gaspard et L’Affect</span> nous ont fait jouer à la **Fête de la Musique Vraiment Super** à Annonay (07)